import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class BookCopyUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('jugthdeApp.bookCopy.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  signatureInput: ElementFinder = element(by.css('input#book-copy-signature'));

  titleInput: ElementFinder = element(by.css('input#book-copy-title'));
}
