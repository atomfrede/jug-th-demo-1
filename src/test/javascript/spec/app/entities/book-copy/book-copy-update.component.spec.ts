/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import BookCopyUpdateComponent from '@/entities/book-copy/book-copy-update.vue';
import BookCopyClass from '@/entities/book-copy/book-copy-update.component';
import BookCopyService from '@/entities/book-copy/book-copy.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('BookCopy Management Update Component', () => {
    let wrapper: Wrapper<BookCopyClass>;
    let comp: BookCopyClass;
    let bookCopyServiceStub: SinonStubbedInstance<BookCopyService>;

    beforeEach(() => {
      bookCopyServiceStub = sinon.createStubInstance<BookCopyService>(BookCopyService);

      wrapper = shallowMount<BookCopyClass>(BookCopyUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          bookCopyService: () => bookCopyServiceStub
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.bookCopy = entity;
        bookCopyServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(bookCopyServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.bookCopy = entity;
        bookCopyServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(bookCopyServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
