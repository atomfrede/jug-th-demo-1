/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import BorrowUpdateComponent from '@/entities/borrow/borrow-update.vue';
import BorrowClass from '@/entities/borrow/borrow-update.component';
import BorrowService from '@/entities/borrow/borrow.service';

import BookCopyService from '@/entities/book-copy/book-copy.service';

import UserService from '@/admin/user-management/user-management.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Borrow Management Update Component', () => {
    let wrapper: Wrapper<BorrowClass>;
    let comp: BorrowClass;
    let borrowServiceStub: SinonStubbedInstance<BorrowService>;

    beforeEach(() => {
      borrowServiceStub = sinon.createStubInstance<BorrowService>(BorrowService);

      wrapper = shallowMount<BorrowClass>(BorrowUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          borrowService: () => borrowServiceStub,

          bookCopyService: () => new BookCopyService(),

          userService: () => new UserService()
        }
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.borrow = entity;
        borrowServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(borrowServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.borrow = entity;
        borrowServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(borrowServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
