/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import BorrowComponent from '@/entities/borrow/borrow.vue';
import BorrowClass from '@/entities/borrow/borrow.component';
import BorrowService from '@/entities/borrow/borrow.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {}
  }
};

describe('Component Tests', () => {
  describe('Borrow Management Component', () => {
    let wrapper: Wrapper<BorrowClass>;
    let comp: BorrowClass;
    let borrowServiceStub: SinonStubbedInstance<BorrowService>;

    beforeEach(() => {
      borrowServiceStub = sinon.createStubInstance<BorrowService>(BorrowService);
      borrowServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<BorrowClass>(BorrowComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          borrowService: () => borrowServiceStub
        }
      });
      comp = wrapper.vm;
    });

    it('should be a Vue instance', () => {
      expect(wrapper.isVueInstance()).toBeTruthy();
    });

    it('Should call load all on init', async () => {
      // GIVEN
      borrowServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllBorrows();
      await comp.$nextTick();

      // THEN
      expect(borrowServiceStub.retrieve.called).toBeTruthy();
      expect(comp.borrows[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      borrowServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeBorrow();
      await comp.$nextTick();

      // THEN
      expect(borrowServiceStub.delete.called).toBeTruthy();
      expect(borrowServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
