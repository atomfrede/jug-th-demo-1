export interface IBookCopy {
  id?: number;
  signature?: string;
  title?: string;
}

export class BookCopy implements IBookCopy {
  constructor(public id?: number, public signature?: string, public title?: string) {}
}
