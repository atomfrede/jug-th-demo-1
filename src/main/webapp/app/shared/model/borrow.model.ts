import { IBookCopy } from '@/shared/model/book-copy.model';
import { IUser } from '@/shared/model/user.model';

export interface IBorrow {
  id?: number;
  borrowedAt?: Date;
  bookcopy?: IBookCopy;
  user?: IUser;
}

export class Borrow implements IBorrow {
  constructor(public id?: number, public borrowedAt?: Date, public bookcopy?: IBookCopy, public user?: IUser) {}
}
