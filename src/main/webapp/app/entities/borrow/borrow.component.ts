import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IBorrow } from '@/shared/model/borrow.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import BorrowService from './borrow.service';

@Component
export default class Borrow extends mixins(Vue2Filters.mixin, AlertMixin) {
  @Inject('borrowService') private borrowService: () => BorrowService;
  private removeId: number = null;
  public borrows: IBorrow[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllBorrows();
  }

  public clear(): void {
    this.retrieveAllBorrows();
  }

  public retrieveAllBorrows(): void {
    this.isFetching = true;

    this.borrowService()
      .retrieve()
      .then(
        res => {
          this.borrows = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IBorrow): void {
    this.removeId = instance.id;
  }

  public removeBorrow(): void {
    this.borrowService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('jugthdeApp.borrow.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllBorrows();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
