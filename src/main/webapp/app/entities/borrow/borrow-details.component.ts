import { Component, Vue, Inject } from 'vue-property-decorator';

import { IBorrow } from '@/shared/model/borrow.model';
import BorrowService from './borrow.service';

@Component
export default class BorrowDetails extends Vue {
  @Inject('borrowService') private borrowService: () => BorrowService;
  public borrow: IBorrow = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.borrowId) {
        vm.retrieveBorrow(to.params.borrowId);
      }
    });
  }

  public retrieveBorrow(borrowId) {
    this.borrowService()
      .find(borrowId)
      .then(res => {
        this.borrow = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
