import { Component, Vue, Inject } from 'vue-property-decorator';

import { IBookCopy } from '@/shared/model/book-copy.model';
import BookCopyService from './book-copy.service';

@Component
export default class BookCopyDetails extends Vue {
  @Inject('bookCopyService') private bookCopyService: () => BookCopyService;
  public bookCopy: IBookCopy = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.bookCopyId) {
        vm.retrieveBookCopy(to.params.bookCopyId);
      }
    });
  }

  public retrieveBookCopy(bookCopyId) {
    this.bookCopyService()
      .find(bookCopyId)
      .then(res => {
        this.bookCopy = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
