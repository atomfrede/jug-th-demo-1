import { mixins } from 'vue-class-component';

import { Component, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IBookCopy } from '@/shared/model/book-copy.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import BookCopyService from './book-copy.service';

@Component
export default class BookCopy extends mixins(Vue2Filters.mixin, AlertMixin) {
  @Inject('bookCopyService') private bookCopyService: () => BookCopyService;
  private removeId: number = null;
  public bookCopies: IBookCopy[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllBookCopys();
  }

  public clear(): void {
    this.retrieveAllBookCopys();
  }

  public retrieveAllBookCopys(): void {
    this.isFetching = true;

    this.bookCopyService()
      .retrieve()
      .then(
        res => {
          this.bookCopies = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IBookCopy): void {
    this.removeId = instance.id;
  }

  public removeBookCopy(): void {
    this.bookCopyService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('jugthdeApp.bookCopy.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllBookCopys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
