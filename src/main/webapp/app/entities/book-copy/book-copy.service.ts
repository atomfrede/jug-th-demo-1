import axios from 'axios';

import { IBookCopy } from '@/shared/model/book-copy.model';

const baseApiUrl = 'api/book-copies';

export default class BookCopyService {
  public find(id: number): Promise<IBookCopy> {
    return new Promise<IBookCopy>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IBookCopy): Promise<IBookCopy> {
    return new Promise<IBookCopy>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IBookCopy): Promise<IBookCopy> {
    return new Promise<IBookCopy>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
