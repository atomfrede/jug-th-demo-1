package com.gitlab.atomfrede.repository;
import com.gitlab.atomfrede.domain.BookCopy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BookCopy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookCopyRepository extends JpaRepository<BookCopy, Long> {

}
